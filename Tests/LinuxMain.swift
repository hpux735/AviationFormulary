//
//  LinuxMain.swift
//  Aviation Formulary
//
//  Created by William Dillon on 3/14/16.
//  See LICENSE in the top-level directory for licensing information
//

import XCTest

@testable import FormularyTestSuite

XCTMain([
    testCase(FormularyTests.allTests)
])

