//
//  Formulary.swift
//  VOR Simulator
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. See LICENSE for more information
//

import Foundation

public func epsilon() -> Float {
    var machineEps: Float = 1.0

    repeat {
        machineEps /= 2.0
    } while ( (1.0 + (machineEps/2.0)) != 1.0)
    
    return machineEps
}

public func epsilon() -> Double {
    var machineEps: Double = 1.0
    
    repeat {
        machineEps /= 2.0
    } while ( (1.0 + (machineEps/2.0)) != 1.0)
    
    return machineEps
}

let eps: Double = epsilon()

public struct Location {
    public var latitude:   Double = 0.0 // Degrees north (positive, south negative)
    public var longitude:  Double = 0.0 // Degrees east  (positive, west  negative)
    public var elevation:  Double = 0.0 // Feet above mean sea level MSL
    
    public static func nullIsland() -> Location {
        return Location(latitude: 0.0, longitude: 0.0, elevation: 0.0)
    }
    
    public init(latitude lat: Double, longitude lon: Double, elevation ele: Double) {
        self.latitude = lat
        self.longitude = lon
        self.elevation = ele
    }
    
    public func toRadians() -> Location {
        return Location(
            latitude:  self.latitude  / 180.0 * M_PI,
            longitude: self.longitude / 180.0 * M_PI,
            elevation: self.elevation)
    }
    
    public func fromRadians() -> Location {
        return Location(
            latitude:  self.latitude  / M_PI * 180.0,
            longitude: self.longitude / M_PI * 180.0,
            elevation: self.elevation)
    }
}

extension Location: CustomStringConvertible {
    public var description: String {
        get {
            return "Latitude: \(latitude), Longitude: \(longitude), Elevation: \(elevation)"
        }
    }
}

public struct Vector {
    public var course:     Double // True course for this leg
    public var distance:   Double // Distance (ground track) for the leg
    public var vertChange: Double // Change in elevation for the leg

    public init(course: Double, distance: Double, vertChange: Double) {
       self.course = course
       self.distance = distance
       self.vertChange = vertChange
    }
}

public prefix func -(vec: Vector) -> Vector {
    return Vector(
        course:     -vec.course,
        distance:    vec.distance, // There's no such thing as negative distance
        vertChange: -vec.vertChange)
}

public func +(lhs: Vector, rhs: Vector) -> Vector {
    assert(false, "Addition of vectors not yet implemented")
    return Vector(course: 0.0, distance: 0.0, vertChange: 0.0)
}

public func /(lhs: Vector, rhs: Double) -> Vector {
    return Vector(course: lhs.course, distance: lhs.distance/rhs, vertChange: lhs.vertChange/rhs)
}

public func mod(value: Double, base: Double) -> Double {
    let integerPart = floor(value / base)
    return value - (integerPart * base)
}

public func +(locDeg: Location, vec: Vector) -> Location {
    let lr = locDeg.toRadians()
    let lat1 = lr.latitude
    let lon1 = lr.longitude
    let   d  = vec.distance * M_PI / (180 * 60)
    let  tc  = vec.course   * M_PI / 180
    
//    let latTerm1 = sin(lat1) // 0.558468985734707
//    let latTerm2 = cos(d)    // 0.999576950689099
//    let latTerm3 = cos(lat1) // 0.829525401643884
//    let latTerm4 = sin(d)    // 0.029084697885367
//    let latTerm5 = cos(tc)   // 0.406688504435716
//    let latTerm6 = latTerm1 * latTerm2 + latTerm3 * latTerm4 * latTerm5
    
    let lat  = asin( sin(lat1) * cos(d) + cos(lat1) * sin(d) * cos(tc) ) // 0.01084916385793
//    let dlon = atan2(sin(tc)   * sin(d) * cos(lat1),  cos(d) - sin(lat1) * sin(lat))
//    let lon = mod(lon1 - dlon + M_PI, M_2_PI) - M_PI

//    let lonTerm1 = sin(tc)
//    let lonTerm2 = sin(d)
//    let lonTerm3 = cos(lat)
//    let lonTerm4 = asin(lonTerm1 * lonTerm2 / lonTerm3) // 0.032264
//    let lonTerm5 = lon1 - lonTerm4 // 2.034206
//    let lonTerm6 = lonTerm5 + M_PI
//    let lonTerm7 = mod(lonTerm6, M_PI * 2.0) - M_PI
    let lon = mod(lon1 + asin(sin(tc) * sin(d) / cos(lat)) + M_PI, base: M_PI * 2.0) - M_PI
    
    return Location(latitude: lat, longitude: lon,
        elevation: locDeg.elevation + vec.vertChange).fromRadians()
}

public func +=(lhs: inout Location, vec: Vector) {
    lhs = lhs + vec
}

// Return the distance between two locations in nautical miles
public func dist(loc1Deg: Location, loc2Deg: Location) -> Double {
    let loc1 = loc1Deg.toRadians()
    let loc2 = loc2Deg.toRadians()
    var lat = sin((loc1.latitude  - loc2.latitude)  / 2.0)
    var lon = sin((loc1.longitude - loc2.longitude) / 2.0)
    let clL = cos(loc1.latitude)
    let clJ = cos(loc2.latitude)
    lat = lat * lat
    lon = lon * lon
    let d = 2.0 * asin(sqrt(lat + clL * clJ * lon))
    return d * 180 * 60 / M_PI
}

// The difference between two locations is the vector from location 1 
// to location 2.  This is computed along the surface of the earth
// without consideration of elevation, then the elevation is linearly
// computed between the given locations.
public func -(loc2Deg: Location, loc1Deg: Location) -> Vector {
    let loc1 = loc1Deg.toRadians()
    let loc2 = loc2Deg.toRadians()
    
    // Compute the distance
    var lat = sin((loc1.latitude  - loc2.latitude)  / 2.0)
    var lon = sin((loc1.longitude - loc2.longitude) / 2.0)
    let clL = cos(loc1.latitude)
    let clJ = cos(loc2.latitude)
    lat = lat * lat
    lon = lon * lon
    let d = 2.0 * asin(sqrt(lat + clL * clJ * lon))
    
    // The formula below does not work if location 1 is a pole
    // Therefore, we can handle it as a special case.
    // Unfortunately, it's basically meaningless.

    // Assume starting from the north pole
    var tc: Double = M_PI
    if cos(loc1.latitude) < eps {
        // Starting from the south pole
        if loc1.latitude < 0.0 {
            tc = 2.0 * M_PI
        }
    }
    // We're not at a pole
    else {
        tc = (sin(loc2.latitude)-sin(loc1.latitude)*cos(d)) / (sin(d)*cos(loc1.latitude))
        tc = acos(tc)
        if sin(loc2.longitude - loc1.longitude) < 0.0 {
            tc = 2.0 * M_PI - tc
        }
    }
    
    // Output value
    return Vector(
        course: tc / M_PI * 180,
        distance: d * 180 * 60 / M_PI,
        vertChange: loc2.elevation - loc1.elevation)
}
